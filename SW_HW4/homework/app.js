function reverseNumber(num) {
  return (
    parseFloat(num.toString().split(``).reverse().join(``)) * Math.sign(num)
  );
}

function forEach(arr, func) {
  for (let i = 0; i < arr.length; i++) {
    func(arr[i]);
  }
}

function map(arr, func) {
  const resultArray = [];
  for (let i = 0; i < arr.length; i++) {
    resultArray.push(func(arr[i], i, arr));
  }
  return resultArray;
}

function filter(arr, func) {
  const resultArray = [];
  for (let i = 0; i < arr.length; i++) {
    if (!!func(arr[i], i, arr)) {
      resultArray.push(arr[i]);
    }
  }
  return resultArray;
}

const ageValue = 18;
const fruitValue = `apple`;

function getAdultAppleLovers(data) {
  return filter(data, (item) => {
    return item.age > ageValue && item.favoriteFruit === fruitValue;
  });
}

function getKeys(obj) {
  const resultArray = [];
  for (const key in obj) {
    if (key) {
      resultArray.push(key);
    }
  }
  return resultArray;
}

function getValues(obj) {
  const resultArray = [];
  for (const key in obj) {
    if (key) {
      resultArray.push(obj[key]);
    }
  }
  return resultArray;
}
