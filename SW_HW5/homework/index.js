function isEquals(x, y) {
  return x === y;
}
function isBigger(x, y) {
  return x > y;
}
function storeNames(...args) {
  return args;
}
function getDifference(x, y) {
  return x > y ? x - y : y - x;
}
function negativeCount(arr) {
  return arr.filter((item) => item < 0).length;
}
function letterCount(str1, str2) {
  return [...str1].filter((item) => item === str2).length;
}
const value = 3;
function countPoints(arr) {
  let count = 0;
  arr.map((item) => {
    const newArr = item.split(`:`);
    if (+newArr[0] > +newArr[1]) {
      count += value;
    } else if (+newArr[0] === +newArr[1]) {
      count++;
    } else {
      count;
    }
    return count;
  });
  return count;
}
