const minAmount = 1000;
const maxYear = 100;
const maxPercent = 100;
const min = 1;
const numberDigits = 2;

let initialAmount = prompt(`Please enter initial amount`);
while (isNaN(initialAmount)) {
  alert(`Invalid input data`);
  initialAmount = prompt(`Please enter initial amount`);
}
while (initialAmount < minAmount) {
  initialAmount = prompt(`Initial amount  can’t be less than 1000`);
}

let numberOfYear = prompt(`Please enter number of years`);
while (isNaN(numberOfYear)) {
  alert(`Invalid input data`);
  numberOfYear = prompt(`Please enter number of years`);
}
while (numberOfYear < min) {
  numberOfYear = prompt(`Number of years  can’t be less than 1`);
}

let percentageOfYear = prompt(`Please enter percentage of`);
while (isNaN(percentageOfYear)) {
  alert(`Invalid input data`);
  percentageOfYear = prompt(`Please enter percentage of`);
}
while (percentageOfYear < min || percentageOfYear > maxYear) {
  percentageOfYear = prompt(`Percentage should be in the range from 1 to 100`);
}

alert(
  `Initial amount: ${initialAmount}
  Number of years: ${numberOfYear}
  Percentage of year: ${percentageOfYear}

  Total profit: ${(
    initialAmount * Math.pow(1 + percentageOfYear / maxPercent, numberOfYear) -
    initialAmount
  ).toFixed(numberDigits)}
  Total amount: ${(
    initialAmount * Math.pow(1 + percentageOfYear / maxPercent, numberOfYear)
  ).toFixed(numberDigits)}
`
);
